/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

#include "framework/mod_lib.h"

namespace air {
namespace link {

class ModManager final {
 public:
  ModManager();
  virtual ~ModManager();

  bool LoadMod(const std::string& dl_path);

  bool RegActor(const std::string& class_name,
                std::function<std::shared_ptr<Actor>(const std::string&)> func);

  bool RegWorker(
      const std::string& class_name,
      std::function<std::shared_ptr<Worker>(const std::string&)> func);

  std::shared_ptr<Actor> CreateActorInst(const std::string& mod_or_class_name,
                                         const std::string& actor_name);

  std::shared_ptr<Worker> CreateWorkerInst(const std::string& mod_or_class_name,
                                           const std::string& worker_name);

 private:
  ModLib lib_mods_;
  std::unordered_map<std::string,
                     std::function<std::shared_ptr<Actor>(const std::string&)>>
      class_actors_;
  std::unordered_map<std::string,
                     std::function<std::shared_ptr<Worker>(const std::string&)>>
      class_workers_;
  pthread_rwlock_t class_actor_rw_;
  pthread_rwlock_t class_worker_rw_;
};

}  // namespace link
}  // namespace air
