/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <list>
#include <memory>
#include <string>

#include "framework/common.h"

namespace air {
namespace link {

class App;
class Msg;
class Actor;
class WorkerCommon;
class Context final : public std::enable_shared_from_this<Context> {
  friend class ContextManager;
  friend class WorkerCommon;
  friend class App;

 public:
  Context(std::shared_ptr<App> app, std::shared_ptr<Actor> mod);
  virtual ~Context() {}

  int Init(const char* param);

  /* actor将消息添加至发送队列中 */
  int SendMsg(std::shared_ptr<Msg> msg);

  /* 工作线程调用回调让actor去处理消息 */
  void Proc(const std::shared_ptr<const Msg>& msg);

  /* 主线程发送消息给该actor */
  void PushMsg(std::shared_ptr<Msg> msg) { recv_.emplace_back(msg); }

  /* 主线程获得该actor待处理消息链表 */
  std::list<std::shared_ptr<Msg>>& GetRecvMsgList() { return recv_; }

  /* 主线程获得该actor发送消息链表 */
  std::list<std::shared_ptr<Msg>>& GetDispatchMsgList() { return send_; }

  void SetRuningFlag(bool in_worker) { in_worker_ = in_worker; }
  bool IsRuning() { return in_worker_; }

  void SetWaitQueueFlag(bool in_wait_queue) { in_wait_que_ = in_wait_queue; }
  bool IsInWaitQueue() { return in_wait_que_; }

  std::shared_ptr<Actor> GetActor() { return actor_; }
  std::shared_ptr<App> GetApp();

  std::string Print();

 private:
  /* 主线程分发给actor的消息链表，主线程操作该链表 */
  std::list<std::shared_ptr<Msg>> recv_;
  /* actor发送给别的actor的消息链表，工作线程处理该actor时可以操作该链表,
   * 空闲时主线程操作该链表 */
  std::list<std::shared_ptr<Msg>> send_;
  /* 该actor的是否在工作线程的标志 */
  bool in_worker_;
  /* actor是否在消息队列中 */
  bool in_wait_que_;
  std::shared_ptr<Actor> actor_;
  std::weak_ptr<App> app_;
};

}  // namespace link
}  // namespace air
