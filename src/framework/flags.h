/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <gflags/gflags.h>

namespace air {
namespace link {

// params
DECLARE_uint32(air_link_worker_count);
DECLARE_uint32(air_link_dispatch_or_process_msg_max);
DECLARE_string(air_link_worker_timer_name);
DECLARE_uint32(air_link_conn_count);

// path
DECLARE_string(air_link_lib_dir);
DECLARE_string(air_link_log_dir);
DECLARE_string(air_link_service_dir);
DECLARE_string(air_link_conf_dir);

}  // namespace link
}  // namespace air
