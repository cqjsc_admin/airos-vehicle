/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/event_conn.h"

#include <sys/socket.h>
#include <sys/types.h>

#include <assert.h>
#include <string>
#include <glog/logging.h>

#include "framework/cutils.h"
#include "framework/msg.h"

namespace air {
namespace link {

EventConn::EventConn() { CreateSockPair(); }

EventConn::~EventConn() { CloseSockPair(); }

int EventConn::GetFd() { return sockpair_[1]; }

EventType EventConn::GetEventType() { return EventType::EVENT_CONN; }

unsigned int EventConn::ListenEpollEventType() { return EPOLLIN; }

void EventConn::RetEpollEventType(uint32_t ev) { ev = ev; }

std::shared_ptr<Msg> EventConn::SendRequest(const std::string& dst,
                                              std::shared_ptr<Msg> req) {
  req->SetSrc(ev_conn_name_);
  req->SetDst(dst);
  send_.clear();
  send_.emplace_back(req);
  SendCmdToMain(WorkerCmd::IDLE);
  WorkerCmd cmd;
  RecvCmdFromMain(&cmd);
  if (recv_.empty()) {
    return nullptr;
  }
  auto msg = recv_.front();
  recv_.clear();
  return msg;
}

int EventConn::SendCmdToWorker(const WorkerCmd& cmd) {
  char cmd_char = static_cast<char>(cmd);
  return write(sockpair_[1], &cmd_char, 1);
}

int EventConn::RecvCmdFromWorker(WorkerCmd* cmd) {
  char cmd_char;
  int ret = read(sockpair_[1], &cmd_char, 1);
  *cmd = (WorkerCmd)cmd_char;
  return ret;
}

int EventConn::RecvCmdFromMain(WorkerCmd* cmd, int timeout_ms) {
  if (timeout_ms < 0) {
    // block
    if (!IsBlock(sockpair_[0])) {
      SetNonBlock(sockpair_[0], false);
    }
  } else if (timeout_ms == 0) {
    // nonblock
    if (IsBlock(sockpair_[0])) {
      SetNonBlock(sockpair_[0], true);
    }
  } else {
    // timeout
    SetSockRecvTimeout(sockpair_[0], timeout_ms);
  }
  char cmd_char;
  int ret = read(sockpair_[0], &cmd_char, 1);
  *cmd = (WorkerCmd)cmd_char;
  return ret;
}

int EventConn::SendCmdToMain(const WorkerCmd& cmd) {
  char cmd_char = static_cast<char>(cmd);
  return write(sockpair_[0], &cmd_char, 1);
}

bool EventConn::CreateSockPair() {
  int res = -1;
  bool ret = true;

  res = socketpair(AF_UNIX, SOCK_DGRAM, 0, sockpair_);
  if (res == -1) {
    LOG(ERROR) << "Worker create sockpair failed";
    return false;
  }
  ret = SetNonBlock(sockpair_[0], false);
  if (!ret) {
    LOG(ERROR) << "Worker set sockpair[0] block failed";
    return ret;
  }
  ret = SetNonBlock(sockpair_[1], false);
  if (!ret) {
    LOG(ERROR) << "Worker set sockpair[1] block failed";
    return ret;
  }
  return ret;
}

void EventConn::CloseSockPair() {
  if (-1 == close(sockpair_[0])) {
    LOG(ERROR) << "Worker close sockpair[0]: " << GetError();
  }
  if (-1 == close(sockpair_[1])) {
    LOG(ERROR) << "Worker close sockpair[1]: " << GetError();
  }
}

void EventConn::SetEvConnName(const std::string& name) { ev_conn_name_ = name; }

std::string EventConn::GetEvConnName() { return ev_conn_name_; }

}  // namespace link
}  // namespace air
