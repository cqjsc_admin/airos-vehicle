/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <pthread.h>
#include <sys/epoll.h>
#include <unistd.h>

#include <atomic>
#include <list>
#include <memory>
#include <string>

#include "framework/event.h"
#include "framework/msg.h"

namespace air {
namespace link {

/**
 * @brief worker与main交互命令
 *  QUIT main主动发起的退出命令
 *  IDLE与RUN为一对请求回复命令
 *  WAIT_FOR_MSG与RUN_WITH_MSG为一对请求回复命令
 */
enum class WorkerCmd : char {
  QUIT = 'q',          ///< worker退出(worker发送的指令)
  IDLE = 'i',          ///< worker空闲(worker发送的指令)
  WAIT_FOR_MSG = 'w',  ///< worker等待消息(worker发送的指令)
  RUN = 'r',           ///< worker运行(main回复的指令)
  RUN_WITH_MSG = 'm',  ///< worker运行(main回复的指令)
};

enum class WorkerCtrlOwner : int {
  MAIN,
  WORKER,
};

class Worker : public Event {
  friend class App;
  friend class ModLib;
  friend class ModManager;
  friend class WorkerManager;

 public:
  Worker();
  virtual ~Worker();

  ////////////////////////////// thread 相关函数
  virtual void OnInit() {}
  virtual void Run() = 0;
  virtual void OnExit() {}
  void Start();
  void Stop();
  bool IsRuning() { return runing_; }
  pthread_t GetPosixThreadId() { return posix_thread_id_; }

  ////////////////////////////// event 相关函数
  int GetFd() override { return sockpair_[1]; }
  unsigned int ListenEpollEventType() override { return EPOLLIN; }
  void RetEpollEventType(uint32_t ev) override { ev = ev; }

  ////////////////////////////// 接收worker/actor消息
  int RecvMsgListSize() { return que_.size(); }
  const std::shared_ptr<const Msg> GetRecvMsg();

  ////////////////////////////// 发送消息相关函数
  int SendMsgListSize() { return send_.size(); }
  void SendMsg(const std::string& dst, std::shared_ptr<Msg> msg);
  void PushSendMsgList(std::list<std::shared_ptr<Msg>>* msg_list);

  ////////////////////////////// 接收/发送主线程控制消息
  /// 不建议使用,除非你知道你在做什么
  int RecvCmdFromMain(WorkerCmd* cmd, int timeout_ms = -1);
  int SendCmdToMain(const WorkerCmd& cmd);
  /// 分发消息并立即返回
  int DispatchMsg();
  /// 分发消息并等待回复消息
  int DispatchAndWaitMsg();

  /// worker fd
  int GetWorkerFd() { return sockpair_[0]; }

  const std::string& GetModName() const { return mod_name_; }
  const std::string& GetTypeName() const { return worker_name_; }
  const std::string& GetInstName() const { return inst_name_; }
  const std::string GetWorkerName() const;

 private:
  static void* ListenThread(void*);

  ////////////////////////////// 线程间通信相关函数
  int SendCmdToWorker(const WorkerCmd& cmd);
  int RecvCmdFromWorker(WorkerCmd* cmd);

  ////////////////////////////// 线程交互控制flag函数
  void SetCtrlOwnerFlag(WorkerCtrlOwner owner) { ctrl_owner_ = owner; }
  WorkerCtrlOwner GetOwner() const { return ctrl_owner_; }
  void SetWaitMsgQueueFlag(bool in_wait_msg_queue) {
    in_msg_wait_queue_ = in_wait_msg_queue;
  }
  bool IsInWaitMsgQueue() { return in_msg_wait_queue_; }

  ////////////////////////////// worker name
  void SetModName(const std::string& name) { mod_name_ = name; }
  void SetTypeName(const std::string& name) { worker_name_ = name; }
  void SetInstName(const std::string& name) { inst_name_ = name; }

  bool CreateSockPair();
  void CloseSockPair();

  /// worker name
  std::string mod_name_;
  std::string worker_name_;
  std::string inst_name_;
  /// idx: 0 used by WorkerCommon, 1 used by App
  int sockpair_[2];
  /// 接收消息队列
  std::list<std::shared_ptr<Msg>> recv_;
  /// 运行时消息队列
  std::list<std::shared_ptr<Msg>> que_;
  /// 发送消息队列
  std::list<std::shared_ptr<Msg>> send_;
  /// posix thread id
  pthread_t posix_thread_id_;
  /// state
  std::atomic_bool runing_;
  WorkerCtrlOwner ctrl_owner_{WorkerCtrlOwner::WORKER};
  bool in_msg_wait_queue_{false};
};

}  // namespace link
}  // namespace air

extern "C" {
typedef std::shared_ptr<air::link::Worker> (*worker_create_func)(
    const std::string&);
}  // extern "C"
