/******************************************************************************
* Copyright 2022 The Airos Authors. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/
#include <memory>
#include <thread>
#include <gtest/gtest.h>

#include "net/udp/udp_impl.h"

TEST(udp, test) {
  std::shared_ptr<air::net::UdpImpl> udp = nullptr;
  air::net::FLAGS_net_udp_server_url = "udp://127.0.0.1:8765";
  air::net::FLAGS_net_udp_local_url = "udp://127.0.0.1:5678";
  udp = air::net::UdpImpl::GetInstance();

  std::thread th([udp]() {
    while (1) {
      std::string src;
      std::string data;
      udp->Recv(&src, &data);
      std::cout << "get from " << src << ": " << data << std::endl;
    }
  });
  while (1) {
    udp->Send("", "hello");
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
}
