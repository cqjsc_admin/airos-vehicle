CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### sub directory
ADD_SUBDIRECTORY(fake_obu_service)
ADD_SUBDIRECTORY(scene_service)
ADD_SUBDIRECTORY(air_api_service)
ADD_SUBDIRECTORY(pc5_service)