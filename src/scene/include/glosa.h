/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once

#include <mutex>
#include <vector>

#include <glog/logging.h>

#include "scene/include/msg_verify.h"

#include "v2xpb-asn-bsm.pb.h"
#include "v2xpb-asn-map.pb.h"
#include "v2xpb-asn-position.pb.h"
#include "v2xpb-asn-spat.pb.h"

namespace v2x {
namespace scene {
class Glosa {
 private:
  bool glosa_active_ = false;
  double maximum_speed_ = -1;
  double minimum_speed_ = -1;
  int recommend_speed_ = -1;
  std::mutex lock_;

 public:
  bool GetGlosaActive();
  double GetMaximumSpeed();
  double GetMinimumSpeed();
  double GetRecommendSpeed();
  bool IsGlosa(const ::v2xpb::asn::Bsm& bsm, const ::v2xpb::asn::MapLane& lane,
               const ::v2xpb::asn::SpatIntersection& spat_intersection);
  double CalculateDistance(const ::v2xpb::asn::Position& pose, double heading,
                           double speed, const ::v2xpb::asn::MapLane& lane);
  void CalculateSpeedRange(
      const std::vector<::v2xpb::asn::SpatPhaseState>& spat_phase_vec,
      double speed, double distance, double speed_limitation);
  void CalculateRecommendSpeed(double speed_limitation);
};
}  // namespace scene
}  // namespace v2x
